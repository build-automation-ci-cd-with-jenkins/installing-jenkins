# Installing-jenkins
  Jenkins installation as a container



## Getting started
   The most popular CI/CD intergrator is Jenkins, which inetegragte with the build tools(maven/Ant/gradle, npm, Docker, k8, terraform, git, remote servers, Ansible etc) to pull source code and it dependencies, test, build, push to dev, and production environment.

The workflow:
- provision a remote server on the cloud provider DigitalOcean
- Open the neccessary ports on which one can login into the remote server - ssh port 22
- ssh into the remote server and update the systems with apt update
- install Jenkins as a container with mounted Volume and open the port 8080 on the container     and 8080 on the server. This is where we are going to access jenkins through the webbrowser.
The outbound rule on the firewall needs to be open to allow.
Create a user and grant access previledge to run the jenkins container. For best practice, it is not advisable to run application on a server as a root user. One needs to use the root user only when installing systems binaries and dependencies.

Attached to this repo, is the jenkins server with some freestyles jonbs




```
cd existing_repo
git remote add origin https://gitlab.com/build-automation-ci-cd-with-jenkins/installing-jenkins.git
git branch -M main
git push -uf origin main
```

